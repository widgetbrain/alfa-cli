import pytest
from alfa_cli.common.helpers.snapshot import compare_output


class TestSnapshot:
    def test_match_object(self):
        expected = {
            "result": 2,
            "goal_score": 715,
            "shifts": [
                {
                    "shift_id": 1,
                    "violation_costs": 0,
                    "user_id": 2,
                    "department_id": "1",
                    "start": 1572186600,
                    "finish": 1572219000,
                    "pay_duration": 510,
                    "breaks": [{"start": 1572193800, "finish": 1572195600}],
                    "shift_costs": 150,
                }
            ],
        }

        output = {
            "result": 2,
            "goal_score": 730,
            "shifts": [
                {
                    "shift_id": 1,
                    "violation_costs": 0,
                    "user_id": 2,
                    "department_id": "1",
                    "start": 1572186600,
                    "finish": 1572219000,
                    "pay_duration": 510,
                    "breaks": [{"start": 1572193800, "finish": 1572195600}],
                    "shift_costs": 170,
                }
            ],
        }

        res = compare_output(output, expected)

        assert res["success"] is False
        assert res["matches"] == 10
        assert res["mismatches"] == 2
        assert len(res["diff"]) == 2
        assert res["diff"][0]["expected"] == 715
        assert res["diff"][0]["received"] == 730
        assert res["diff"][1]["expected"] == 150
        assert res["diff"][1]["received"] == 170

    def test_match_string(self):
        expected = "hello"
        output = "world"

        res = compare_output(output, expected)

        assert res["success"] is False
        assert res["mismatches"] == 1
        assert res["diff"][0]["expected"] == "hello"
        assert res["diff"][0]["received"] == "world"

    def test_match_array(self):
        expected = {"values": [1, 2, 3, 4]}
        output = {"values": [1, 2, 3]}

        res = compare_output(output, expected)

        assert res["mismatches"] == 1
        assert res["similarity"] == 0.75
        assert res["diff"][0]["expected"] == 4
        assert res["diff"][0]["received"] is None

    def test_match_int(self):
        expected = 20
        output = 20

        res = compare_output(output, expected)

        assert res["success"] is True
        assert len(res["diff"]) == 0

    def test_match_undefined(self):
        expected = {"a": 1, "b": 2, "c": {"1": 1, "2": 2}, "d": [1, 2]}
        output = {"a": 1, "b": 2}

        res = compare_output(output, expected)

        assert res["success"] is False
        assert len(res["diff"]) > 2

    def test_match_wildcard_object(self):
        expected = {"property1": "*", "property2": 10}
        output = {"property1": {"a": 1, "b": 2}, "property2": 10}

        res = compare_output(output, expected)

        assert res["success"] is True
        assert len(res["diff"]) == 0

    def test_match_wildcard_string(self):
        expected = "*"
        output = "something"

        res = compare_output(output, expected)

        assert res["success"] is True
        assert len(res["diff"]) == 0

    def test_match_wildcard_undefined(self):
        expected = {"property1": "*", "property2": 10}
        output = {"property2": 10}

        res = compare_output(output, expected)

        assert res["success"] is False
        assert len(res["diff"]) == 1

    def test_match_empty_object(self):
        expected = {"output": {}}
        output = {"output": {"key": "value"}}

        res = compare_output(output, expected)

        assert res["success"] is False
        assert len(res["diff"]) == 1

    def test_match_empty_array(self):
        expected = {"output": []}
        output = {"output": [1, 2]}

        res = compare_output(output, expected)

        assert res["success"] is False
        assert len(res["diff"]) == 1

    def test_match_empty_array_success(self):
        expected = {"output": []}
        output = {"output": []}

        res = compare_output(output, expected)

        assert res["success"] is True

    def test_match_empty_array_undefined_output(self):
        expected = {"output": {}}
        output = {}

        res = compare_output(output, expected)

        assert res["success"] is False
        assert res["mismatches"] == 1

