import pytest
import os
from alfa_cli.common.helpers.file import extract_dir


class TestReader:
    def test_extract_dir(self):
        path = os.path.join(".", "tests", "data", "algorithm", "tests")
        files = extract_dir(path)

        assert len(files) == 5

    def test_extract_dir_subdir(self):
        path = os.path.join(".", "tests", "data", "algorithm", "tests")
        files = extract_dir(path, prefix='specific')

        assert len(files) == 4

    def test_extract_dir_ext(self):
        path = os.path.join(".", "tests", "data", "algorithm", "tests")
        files = extract_dir(path, ext='txt')

        assert len(files) == 1

    def test_extract_dir_subdir_ext(self):
        path = os.path.join(".", "tests", "data", "algorithm", "tests")
        files = extract_dir(path, prefix='specific', ext='json')

        assert len(files) == 3