import pytest
import os
from alfa_sdk.common.helpers import AlfaConfigHelper
from alfa_cli.common.helpers.file import resolve_package_files
from alfa_cli.common.helpers.ignore import extract_ignore_rules, DEFAULT_EXCLUDES


class TestIgnore:
    @pytest.fixture(scope="class")
    def dir_path(self):
        return os.path.join(os.getcwd(), "tests", "data", "test-ignore")

    @pytest.fixture(scope="class")
    def conf(self, dir_path):
        file_path = os.path.join(dir_path, "alfa.yml")
        return AlfaConfigHelper.load(file_path)

    class TestIgnoreRules:
        def test_default(self):
            # ACT
            excludes, includes = extract_ignore_rules()

            # ASSERT
            assert set(excludes) == set(DEFAULT_EXCLUDES)
            assert includes == []

        def test_ignore_function(self, conf):
            conf_functions = {"functions": conf["functions"]}
            expected_excludes = ["by_subdir/**/ignored/**/*", "by_type/**/*.txt"]

            # ACT
            excludes, includes = extract_ignore_rules(conf=conf_functions)

            # ASSERT
            assert sorted(excludes) == sorted(expected_excludes + DEFAULT_EXCLUDES)
            assert includes == []

        def test_ignore_root(self, conf):
            conf_root = {"package": conf["package"]}
            expected_excludes = ["by_subdir/**/ignored/**/*", "by_dir/**/*"]
            expected_includes = ["by_dir/included.json", "by_dir/dir/included.json"]

            # ACT
            excludes, includes = extract_ignore_rules(conf=conf_root)

            # ASSERT
            assert sorted(excludes) == sorted(expected_excludes + DEFAULT_EXCLUDES)
            assert sorted(includes) == sorted(expected_includes)

        def test_ignore_both(self, conf):
            expected_excludes = ["by_subdir/**/ignored/**/*", "by_dir/**/*", "by_type/**/*.txt"]
            expected_includes = ["by_dir/included.json", "by_dir/dir/included.json"]

            # ACT
            excludes, includes = extract_ignore_rules(conf=conf)

            # ASSERT
            assert sorted(excludes) == sorted(expected_excludes + DEFAULT_EXCLUDES)
            assert sorted(includes) == sorted(expected_includes)

        def test_ignore_integration(self):
            # ARRANGE
            dir_path = os.path.join(os.getcwd(), "tests", "data", "test-ignore-integration")
            conf_path = os.path.join(dir_path, "alfa.yml")
            conf = AlfaConfigHelper.load(conf_path)

            # ACT
            excludes, includes = extract_ignore_rules(conf=conf, function_type="integration")

            # ASSERT
            expected_excludes = [
                "app/**/node_modules/**/*",
                "app/**/ignore.json",
                "app/lib/ignore.json",
            ]
            assert sorted(excludes) == sorted(expected_excludes + DEFAULT_EXCLUDES)
            assert includes == []

    class TestIgnoreResolve:
        def test_resolve(self, conf, dir_path):
            # ACT
            excludes, includes = extract_ignore_rules(conf=conf)
            res = resolve_package_files(dir_path, excludes=excludes, includes=includes)

            # ASSERT
            files = [path for path in res if not path.is_dir()]
            stems = [str(file.stem) for file in files if str(file.stem) != "alfa"]
            assert all(stem == "included" for stem in stems)
            assert "excluded" not in stems
