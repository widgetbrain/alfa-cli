def search(searchOptions, data, buildArguments, scores=None):
    if not scores:
        result = []
        for method in searchOptions["method"]:
            result.append({"method": method, "data": data, "arg1": "bar"})

        return {"score": result}

    return {"build": scores[0]["parameters"]}
