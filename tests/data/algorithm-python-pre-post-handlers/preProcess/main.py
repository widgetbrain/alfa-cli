def run(problem):
    value = problem.get("value")
    new_value = value * 2
    return {"value": new_value}
