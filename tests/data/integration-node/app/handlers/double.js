const { multiply } = require('../lib/math_func');

exports.run = function run(x) {
  return multiply(x, 2);
}
