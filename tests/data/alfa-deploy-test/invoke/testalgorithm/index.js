exports.run = function(value) {
  const newValue = value * 2;
  const res = { 
    in: value, 
    out: newValue,
  };

  return res;
}
