from lib.math_func import multiply

def run(x):
    return multiply(x, 2)
