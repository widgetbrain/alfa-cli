exports.run = function run(value) {
    const newValue = value * 2;
    return {
      in: value,
      out: newValue,
    };
};