import pytest
import os
import json
import wb_parameter_handler.main as wb_parameter_handler
from alfa_sdk.common.helpers import EndpointHelper
from alfa_sdk.common.session import fetch_alfa_env, fetch_setting

from click.testing import CliRunner
from alfa_cli.cli import alfa as cli


@pytest.fixture(scope="session")
def credentials():
    client_id = "/alfa/development/user/1683/auth0/clientId"
    client_secret = "/alfa/development/user/1683/auth0/clientSecret"

    test_params = wb_parameter_handler.load_parameters(
        [client_id, client_secret], path="/tmp/simple.cache"
    )

    return {"client_id": test_params[client_id], "client_secret": test_params[client_secret]}


@pytest.fixture(scope="session")
def endpoint():
    alfa_env = fetch_alfa_env()
    alfa_id = fetch_setting("alfa_id")
    region = fetch_setting("platform_region")
    return EndpointHelper(alfa_env=alfa_env, alfa_id=alfa_id, region=region)


@pytest.fixture(scope="session")
def alfa():
    class Runner:
        def __init__(self):
            self.runner = CliRunner()

        def invoke(self, *args, parse=True, **kwargs):
            res = self.runner.invoke(cli, *args, **kwargs)
            if res.exception:
                raise res.exception

            data = res.output
            if parse:
                try:
                    data = json.loads(data)
                except:
                    print("'{}'".format(data))
                    raise
            return data

    return Runner()


@pytest.fixture(scope="session", autouse=True)
def configure(credentials, alfa):
    os.environ["ALFA_CLIENT_ID"] = credentials.get("client_id")
    os.environ["ALFA_CLIENT_SECRET"] = credentials.get("client_secret")

    alfa.invoke(["configure", "--alfa-env", "dev", "--no-verbose"], parse=False)
