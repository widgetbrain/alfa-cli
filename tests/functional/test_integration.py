import json
import os
import subprocess
import pytest
import responses
from alfa_cli.common.exceptions import InvocationError


class TestIntegration:
    @pytest.fixture(scope="class")
    def constants(self):
        return {
            "integration_id": "9a882618-336e-4988-b85c-dc98b7e13c4f:alfa-sdk",
            "environment_name": "alfa-sdk",
            "function_name": "double",
        }

    @pytest.fixture(scope="function", autouse=True)
    def cleanup(self):
        root = os.getcwd()
        yield
        os.chdir(root)

    @responses.activate
    def test_invoke(self, alfa, constants):
        responses.add_passthru(
            "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp?appId=30"
        )
        responses.add(
            responses.POST,
            "https://ais-dev.widgetbrain.io/api/Integrations/9a882618-336e-4988-b85c-dc98b7e13c4f:alfa-sdk/environments/alfa-sdk/functions/double/invoke",
            status=200,
            json={"out": 140},
        )

        payload = {"value": 70}
        res = alfa.invoke(
            [
                "integration",
                "invoke",
                constants["integration_id"],
                constants["environment_name"],
                constants["function_name"],
                json.dumps(payload),
            ]
        )

        assert res == {"out": 140}

        invoke_requests = [
            call.request for call in responses.calls if "integrations" in call.request.url.lower()
        ]
        assert len(invoke_requests) == 1
        invoke_body = json.loads(invoke_requests[0].body)
        assert invoke_body == payload

    @responses.activate
    def test_invoke_path(self, alfa, constants):
        responses.add_passthru(
            "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp?appId=30"
        )
        responses.add(
            responses.POST,
            "https://ais-dev.widgetbrain.io/api/Integrations/9a882618-336e-4988-b85c-dc98b7e13c4f:alfa-sdk/environments/alfa-sdk/functions/double/invoke",
            status=200,
            json={"out": 140},
        )

        payload = {"value": 70}
        path = "/tmp/alfa-cli-integration-payload.json"
        open(path, "w").write(json.dumps(payload))

        res = alfa.invoke(
            [
                "integration",
                "invoke",
                constants["integration_id"],
                constants["environment_name"],
                constants["function_name"],
                path,
            ]
        )
        assert res == {"out": 140}

        invoke_requests = [
            call.request for call in responses.calls if "integrations" in call.request.url.lower()
        ]
        assert len(invoke_requests) == 1
        invoke_body = json.loads(invoke_requests[0].body)
        assert invoke_body == payload

    def test_invoke_local_python(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-python")
        os.chdir(path)

        problem = {"x": 15}
        res = alfa.invoke(["integration", "invoke-local", "-f", "double", json.dumps(problem)])

        assert res == 30

    def test_invoke_local_python_root(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-python")
        os.chdir(path)

        problem = {"x": 57, "y": 26}
        res = alfa.invoke(["integration", "invoke-local", "-f", "multiply", json.dumps(problem)])

        assert res == 1482

    def test_invoke_local_python_work_dir(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-python-work-dir")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["integration", "invoke-local", "-f", "handler", json.dumps(problem)])

        assert res == os.path.join(os.getcwd(), "app")

    def test_invoke_local_python_work_dir_root(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-python-work-dir")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["integration", "invoke-local", "-f", "lib", json.dumps(problem)])

        assert res == os.path.join(os.getcwd(), "app/lib")

    def test_invoke_local_node(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-node")
        os.chdir(path)

        problem = {"x": 15}
        res = alfa.invoke(["integration", "invoke-local", "-f", "double", json.dumps(problem)])

        assert res == 30

    def test_invoke_local_node_root(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "integration-node")
        os.chdir(path)

        problem = {"x": 57, "y": 26}
        res = alfa.invoke(["integration", "invoke-local", "-f", "multiply", json.dumps(problem)])

        assert res == 1482

    def test_invoke_local_node_unhandled_rejection(self, alfa):
        # Arrange
        path = os.path.join(os.getcwd(), "tests", "data", "integration-node-unhandled-rejection")
        os.chdir(path)

        problem = {"x": 15}

        # Act & Assert
        with pytest.raises(InvocationError):
            alfa.invoke(["integration", "invoke-local", "-f", "main", json.dumps(problem)])
