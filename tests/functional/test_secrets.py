import pytest
import uuid


class TestSecrets:
    @pytest.fixture(scope="class")
    def name(self):
        name = "{}-{}".format("test", str(uuid.uuid4()))
        return name

    def test_store_value(self, alfa, name):
        res = alfa.invoke(["secrets", "store-value", name, "test_value_cli_test"])
        assert res["name"] == name

    def test_fetch_value(self, alfa, name):
        res = alfa.invoke(["secrets", "fetch-value", name])
        assert res == "test_value_cli_test"

    def test_list_names(self, alfa, name):
        res = alfa.invoke(["secrets", "list-names"])
        assert type(res) is list

        names = [x["name"] for x in res]
        assert (name in names) is True

    def test_delete_value(self, alfa, name):
        res = alfa.invoke(["secrets", "remove-value", name])
        assert res["count"] == 1
