import os
import re
import uuid
import pytest
import responses
from unittest.mock import patch

from alfa_sdk import AlgorithmClient
from alfa_sdk.common.utils import format_environment_id


#


def assert_filename(body, filename_pattern):
    file_header = f'Content-Disposition: form-data; name="file"; filename="{filename_pattern}"'
    occurence = re.findall(re.compile(file_header.encode("utf_8")), body)
    assert len(occurence) == 1


#


class TestDeployment:
    @pytest.fixture(scope="class")
    def constants(self):
        name = str(uuid.uuid4()).split("-")[0]
        team_id = "9a882618-336e-4988-b85c-dc98b7e13c4f"
        algorithm_id = "bb1d784a-bc4d-4f8a-9805-25d8a38baa29"

        return {
            "team_id": team_id,
            "algorithm_id": algorithm_id,
            "environment_id": format_environment_id(team_id, algorithm_id, name),
            "environment_name": name,
            "environment_id_source": format_environment_id(team_id, algorithm_id, "alfa-sdk"),
            "environment_name_source": "alfa-sdk",
            "filepath_algorithm": os.path.join(os.getcwd(), "tests", "data", "alfa-deploy-test"),
            "filepath_integration": os.path.join("tests", "data", "integration-python"),
        }

    #

    def test_deploy_algorithm(self, alfa, constants):
        res = alfa.invoke(
            [
                "algorithm",
                "deploy",
                "-i",
                constants["algorithm_id"],
                "-e",
                constants["environment_name"],
                "--commit",
                "CLI TEST",
                constants["filepath_algorithm"],
                "--increment",
            ]
        )
        assert res["algorithmEnvironmentId"] == constants["environment_id"]

        client = AlgorithmClient()
        environment = client.get_environment(constants["environment_id"])
        environment.delete()

    @responses.activate
    @patch("alfa_sdk.resources.algorithm.Algorithm._fetch_data")
    @patch("alfa_sdk.resources.algorithm.AlgorithmEnvironment._fetch_data")
    def test_deploy_algorithm_to_other_team(
        self, get_environment, get_algorithm, alfa, constants, endpoint
    ):
        # ARRANGE
        name = constants["environment_name"]
        team_id = str(uuid.uuid4())

        get_algorithm.return_value = {}
        get_environment.return_value = {}

        responses.add_passthru(endpoint.resolve("core"))
        responses.add(
            responses.POST,
            endpoint.resolve("release", "/api/Releases/upload"),
            status=200,
            json={"id": "-1"},
        )

        # ACT
        res = alfa.invoke(
            [
                "algorithm",
                "deploy",
                "-i",
                constants["algorithm_id"],
                "-e",
                constants["environment_name"],
                "--commit",
                "CLI TEST",
                constants["filepath_algorithm"],
                "--team-id",
                team_id,
            ]
        )

        # ASSERT
        get_algorithm.assert_called()
        get_environment.assert_called()

        upload_requests = [
            call.request for call in responses.calls if "/api/Releases/upload" in call.request.url
        ]
        assert_filename(upload_requests[0].body, "alfa-deploy-.{0,10}.zip")

        assert len(upload_requests) == 1
        assert upload_requests[0].params == {
            "environmentId": format_environment_id(team_id, constants["algorithm_id"], name),
            "type": "algorithm",
            "version": "0.0.1",
            "description": "this is a test [CLI TEST]",
        }

    @responses.activate
    @patch("alfa_sdk.resources.integration.Integration._fetch_data")
    @patch("alfa_sdk.resources.integration.IntegrationEnvironment._fetch_data")
    def test_deploy_integration(self, get_integration, get_environment, alfa, constants, endpoint):
        # Arrange
        integration_id = str(uuid.uuid4())
        environment_name = str(uuid.uuid4())

        get_integration.return_value = {}
        get_environment.return_value = {}

        responses.add_passthru(endpoint.resolve("core"))
        responses.add(
            responses.POST,
            endpoint.resolve("release", "/api/Releases/upload"),
            status=200,
            json={"id": -1},
        )
        responses.add(
            responses.GET,
            endpoint.resolve("release", "/api/Releases/getList"),
            match_querystring=False,
            status=200,
            json={},
        )

        # Act
        alfa.invoke(
            [
                "integration",
                "deploy",
                "-i",
                integration_id,
                "-e",
                environment_name,
                "--commit",
                "CLI TEST",
                constants["filepath_integration"],
            ]
        )

        # Assert
        upload_requests = [
            call.request for call in responses.calls if "/api/Releases/upload" in call.request.url
        ]
        assert_filename(upload_requests[0].body, "ais-deploy-.{0,10}.zip")

        assert len(upload_requests) == 1
        assert upload_requests[0].params == {
            "environmentId": format_environment_id(
                constants["team_id"], integration_id, environment_name
            ),
            "type": "integration",
            "version": "0.1.0",
            "description": " [CLI TEST]",
        }
