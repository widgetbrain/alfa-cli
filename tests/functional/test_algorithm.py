import json
from numbers import Number
import os
import pickle
import shutil
import pytest


from alfa_sdk import AlgorithmClient


class TestAlgorithm:
    @pytest.fixture(scope="class")
    def constants(self):
        return {
            "algorithm_id": "bb1d784a-bc4d-4f8a-9805-25d8a38baa29",
            "environment_id": "9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk",
            "environment_name": "alfa-sdk",
        }

    @pytest.fixture(scope="function", autouse=True)
    def cleanup(self):
        root = os.getcwd()
        yield
        os.chdir(root)

    #

    def test_invoke(self, alfa, constants):
        problem = {"value": 117}
        res = alfa.invoke(
            [
                "algorithm",
                "invoke",
                constants["algorithm_id"],
                constants["environment_name"],
                json.dumps(problem),
            ]
        )
        assert res["out"] == 234

    def test_invoke_path(self, alfa, constants):
        problem = {"value": 117}
        path = "/tmp/alfa-cli-data.json"
        open(path, "w").write(json.dumps(problem))

        res = alfa.invoke(
            ["algorithm", "invoke", constants["algorithm_id"], constants["environment_name"], path]
        )
        assert res["out"] == 234

    def test_invoke_local_python(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        problem = {"value": 117}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res["out"] == 234

    def test_invoke_local_node(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-node")
        os.chdir(path)

        problem = {"value": 117}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res["out"] == 234

    def test_invoke_local_path(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        problem = {"value": 117}
        path = "/tmp/alfa-cli-data.json"
        open(path, "w").write(json.dumps(problem))

        res = alfa.invoke(["algorithm", "invoke-local", path])

        assert res["out"] == 234

    def test_invoke_local_custom_spec(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-custom-spec")
        os.chdir(path)

        problem = {"x": 10, "y": 12}
        res = alfa.invoke(["algorithm", "invoke-local", "--spec", "spec.yml", json.dumps(problem)])

        assert res == 120

    def test_invoke_local_python_no_arguments(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-no-arguments")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res == "triggered something!"

    def test_invoke_local_node_no_arguments(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-node-no-arguments")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res == "triggered something!"

    def test_invoke_local_python_working_directory(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-work-dir")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res == os.path.join(os.getcwd(), "invoke")

    def test_invoke_local_context(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-context")
        os.chdir(path)

        problem = {}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res["__RUN_LOCAL__"] == True
        assert "token" in res
        assert "userId" in res
        assert "teamId" in res

    def test_invoke_tests(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        res = alfa.invoke(["algorithm", "invoke-tests"])

        assert res["success"] is True
        assert res["passed"] == 2

    def test_invoke_tests_error(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        res = alfa.invoke(["algorithm", "invoke-tests", "-e", "error"])

        assert res["success"] is False
        assert res["passed"] == 2
        assert res["failed"] == 1

    def test_train_local_method(self, alfa):
        # Arrange
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        tag = "test"
        data = json.dumps({"x": 14, "y": 42})
        build_configuration = json.dumps({"methods": ["foo", "bar", "baz"]})

        # Act
        res = alfa.invoke(
            ["algorithm", "train-local", "--tag", tag, "--data", data, "-bc", build_configuration]
        )

        # Assert
        assert isinstance(res, dict)
        assert res["file"].startswith(
            os.path.join(
                "build",
                ".instances",
                "9a882618-336e-4988-b85c-dc98b7e13c4f",
                "e3a5af2d-e713-4a97-a675-593b3d0cf1d1",
                "develop",
                "test",
            )
        )
        assert res["properties"] == {}
        assert res["method"] in ["foo", "bar", "baz"]
        assert isinstance(res["score"], Number)
        assert os.path.isfile(res["file"])
        with open(res["file"], "rb") as instance_file:
            instance = pickle.load(instance_file)
            assert instance == "hello world"

        shutil.rmtree("build/.instances")

    def test_train_local_search(self, alfa):
        # Arrange
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm")
        os.chdir(path)

        tag = "test"
        data = json.dumps({"x": 14, "y": 42})
        build_configuration = json.dumps(
            {"searchOptions": {"method": ["foo", "bar", "baz"]}, "arguments": {"arg1": "foo"}}
        )

        # Act
        res = alfa.invoke(
            ["algorithm", "train-local", "--tag", tag, "--data", data, "-bc", build_configuration]
        )

        # Assert
        assert isinstance(res, dict)
        assert res["file"].startswith(
            str(
                os.path.join(
                    "build",
                    ".instances",
                    "9a882618-336e-4988-b85c-dc98b7e13c4f",
                    "e3a5af2d-e713-4a97-a675-593b3d0cf1d1",
                    "develop",
                    "test",
                )
            )
        )
        assert res["properties"] == {}
        assert res["method"] == "foo"
        assert isinstance(res["buildOptions"], dict)
        assert len(res["buildOptions"].keys()) == 1
        assert "method" in res["buildOptions"].keys()
        assert isinstance(res["scores"], list)
        assert len(res["scores"]) == 3
        assert res["scores"][0]["options"] == {"method": "foo"}
        assert isinstance(res["scores"][0]["result"], Number)
        assert os.path.isfile(res["file"])
        with open(res["file"], "rb") as instance_file:
            instance = pickle.load(instance_file)
            assert instance == "hello world"

        shutil.rmtree("build/.instances")

    def test_invoke_local_python_pre_post_handlers(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-python-pre-post-handlers")
        os.chdir(path)

        problem = {"value": 2}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res["in"] == 4
        assert res["out"] == 8
        assert res["postProcessed"] == True

    #

    def test_invoke_local_node_pre_post_handlers(self, alfa):
        path = os.path.join(os.getcwd(), "tests", "data", "algorithm-node-pre-post-handlers")
        os.chdir(path)

        problem = {"value": 2}
        res = alfa.invoke(["algorithm", "invoke-local", json.dumps(problem)])

        assert res["in"] == 4
        assert res["out"] == 8
        assert res["postProcessed"] == True