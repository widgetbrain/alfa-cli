from alfa_sdk.common.exceptions import AlfaError


class AlfaCliError(AlfaError):
    template = "{message}"


class RuntimeError(AlfaCliError):
    pass

class TestError(AlfaCliError):
    pass

class InvocationError(AlfaCliError):
    pass